import Vue from 'vue'
import Tip from '../components/Tip.vue'
import Sooclogin from '../components/Sooclogin.vue'

const components = { Tip,Sooclogin }

Object.keys(components).forEach(key => {
  Vue.component(key, components[key])
})
